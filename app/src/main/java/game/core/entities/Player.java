package game.core.entities;

import com.almasb.fxgl.entity.Entity;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static com.almasb.fxgl.dsl.FXGL.*;

public class Player extends Entity {
    private static Player instance;

    public final int SPEED = 5;

    public int x, y;

    public char direction = 'd';

    private Player() {
        super();
        x = y = 20;
    }

    public static Player getInstance() {
        if (instance == null) {
            instance = new Player();
        }
        return instance;
    }

    public Entity getEntity() {
        return entityBuilder()
                .at(x, y)
                .view(new Rectangle(25, 25, Color.BLUE))
                .buildAndAttach();
    }

}
