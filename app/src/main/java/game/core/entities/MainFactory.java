package game.core.entities;

import static com.almasb.fxgl.dsl.FXGL.*;
import com.almasb.fxgl.entity.*;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class MainFactory implements EntityFactory {
    private static MainFactory instance;
    private Player player = Player.getInstance();

    private MainFactory() {
        super();
    }

    public static MainFactory getInstance() {
        if (instance == null) {
            instance = new MainFactory();
        }
        return instance;
    }

    @Spawns("player")
    public Entity playerEntity(SpawnData data) {
        return entityBuilder().at(player.x, player.y)
                .view(new Rectangle(25, 25, Color.BLUE))
                .buildAndAttach();
    }
}
